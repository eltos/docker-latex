FROM debian:testing


RUN adduser \
  --home "/home/latex" \
  --uid 1000 \
  --gecos "LaTEX" \
  --disabled-password \
  "latex"


RUN apt-get update && apt-get install -y \
  # latex and pygmentize for minted package
  texlive-full python3-pygments \
  # Remove unnecessary stuff
  && apt-get --purge remove -y .\*-doc$ \
  && apt-get clean -y \
  && rm -rf /var/lib/apt/lists/*
  