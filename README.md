# Latex Docker Image

## About
Docker image with texlive to generate PDF files

## Usage

```yml
  image: registry.gitlab.com/eltos/docker-latex
```

See **[Registry](https://gitlab.com/eltos/docker-latex/container_registry)** for all builds.

See **[Snippets](https://gitlab.com/eltos/docker-latex/-/snippets)** for extended usage examples.

